package Practicas;

public class Persona {
    //Atributo estatico de la clase
    private static int nPersonas;
    //Cada instancia incrementa este atributo
    public Persona() {
        nPersonas++;
    }
    //Metodo estatico que retorna un atributo estatico
    public static int getNPersonas() {
        return nPersonas;
    }
    //MAIN
    public static void main(String[] args) {
        //Se crean instancias
        Persona p1 = new Persona();
        Persona p2 = new Persona();
        Persona p3 = new Persona();
        Persona p4 = new Persona();
        //Accedemos al metodo estatico para ver el numero
        //de instancias de tipo Persona creadas
        System.out.println(Persona.getNPersonas());
    }
    
    //5:21
}