package Practicas;

public class singleton {
	
	String nombre;
	public static singleton instancia=null;

	private singleton(String nombre) {
		this.nombre=nombre;
		
		System.out.println("soy la instancia de: "+ nombre);
	}
	
	public static singleton getSingleton(String nombre) {
		
		if(instancia==null) {
			instancia=new singleton(nombre);
		}else {
			System.out.println("no se puede crear la instancia de "+ nombre);
		}
		
		return instancia;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//singleton p= new singleton("j");
		//singleton pk= new singleton("jj");
		singleton prueba= singleton.getSingleton("jose");
		singleton prueba2= singleton.getSingleton("manuel");
		
		System.out.println(prueba.nombre);
		System.out.println(prueba2.nombre);
		/*System.out.println(p.nombre);
		System.out.println(pk.nombre);*/

	}

}
